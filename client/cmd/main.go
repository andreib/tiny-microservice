package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
	"microservice/model"
	"microservice/server/calls"
	"strconv"
	"strings"
	"time"
)

func main() {
	log, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}

	con, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		panic(err)
	}

	req := &model.HelloWorldRequest{
		Name: "Mrs Miggins",
	}

	res := &model.HelloWorldResponse{}

	if err := callAPI(con, calls.HelloWorld, req, res); err != nil {
		panic(err)
	}

	log.Info(res.Result)

}

func callAPI[T proto.Message, U proto.Message](con *nats.Conn, subject string, command T, ret U) error {
	b, err := proto.Marshal(command)
	if err != nil {
		return err
	}
	msg := nats.NewMsg(subject)
	msg.Data = b
	res, err := con.Request(subject, b, time.Second*60)
	if err != nil {
		if err == nats.ErrNoResponders {
			err = fmt.Errorf("shar-client: shar server is offline or missing from the current nats server")
		}
		return err
	}
	if len(res.Data) > 4 && string(res.Data[0:4]) == "ERR_" {
		em := strings.Split(string(res.Data), "_")
		e := strings.Split(em[1], "|")
		i, err := strconv.Atoi(e[0])
		if err != nil {
			i = 0
		}
		return &ApiError{Code: i, Message: e[1]}
	}
	if err := proto.Unmarshal(res.Data, ret); err != nil {
		return err
	}
	return nil
}

// ApiError returns a GRPC error code and an error message
type ApiError struct {
	Code    int
	Message string
}

func (a ApiError) Error() string {
	return fmt.Sprintf("code %d: %s", a.Code, a.Message)
}
