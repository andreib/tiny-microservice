package main

import (
	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
	"microservice/server/api"
	"time"
)

func main() {
	log, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}

	con, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		panic(err)
	}

	svr, err := api.New(log, con, false)
	if err != nil {
		panic(err)
	}

	if err := svr.Listen(); err != nil {
		panic(err)
	}

	// About here you would wait for a SIGTERM, but I'm too lazy to do this here, and it would increase the complexity
	time.Sleep(24 * time.Hour)
}
