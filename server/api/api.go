package api

import (
	"context"
	"microservice/model"
	"microservice/server/calls"
)

func (s *TinyMicroService) Listen() error {
	log := s.log

	if _, err := listen(s.con, log, s.panicRecovery, s.subs, calls.HelloWorld, &model.HelloWorldRequest{}, s.helloWorld); err != nil {
		return err
	}

	return nil
}

func (s *TinyMicroService) helloWorld(ctx context.Context, request *model.HelloWorldRequest) (*model.HelloWorldResponse, error) {
	return &model.HelloWorldResponse{
		Result: "Hello " + request.Name + " from your API",
	}, nil
}
