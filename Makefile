default: proto server

configure:
	@echo "\033[92mConfigure\033[0m"
	go version
	go get -d google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
	go get -d google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
	go get -d github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest

all: proto server

proto: .FORCE
	@echo "\033[92mBuild proto\033[0m"
	cd proto; protoc --go_opt=M=gitlab.com --go_out=../model tiny-microservice/models.proto;
	mv model/gitlab.com/tiny-microservice/model/models.pb.go model/
	rm -rf model/gitlab.com
test: proto server .FORCE
	go test --short --race ./...
	golangci-lint cache clean
	golangci-lint run -v -E gosec -E ireturn --timeout 5m0s
.FORCE:
